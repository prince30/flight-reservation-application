package com.desh.flightreservation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.desh.flightreservation.dto.ReservationRequest;
import com.desh.flightreservation.model.Flight;
import com.desh.flightreservation.model.Reservation;
import com.desh.flightreservation.service.FlightService;
import com.desh.flightreservation.service.ReservationService;


@Controller
public class ReservationController {

	@Autowired
	private FlightService flightService;
	
	@Autowired
	private ReservationService reservationService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(value = "/showCompleteReservation", method = RequestMethod.GET)
	public String showCompleteReservation(@RequestParam("flightId") Long flightId, ModelMap modelMap) {
		LOGGER.info("showCompleteReservation() invoked with the Flight  Id: " + flightId);
		Flight flight = flightService.getById(flightId);
		modelMap.addAttribute("flight", flight);
		return "reservations/completeReservation";
	}

	@RequestMapping(value = "/completeReservation", method = RequestMethod.POST)
	public String completeReservation(ReservationRequest request, ModelMap modelMap) {
		LOGGER.info("completeReservation() " + request);
		Reservation reservation = reservationService.bookFlight(request);
		modelMap.addAttribute("message", "Reservation has created successfully and the id is : " + reservation.getId());
		return "reservations/reservationConfirmation";
	}
}
