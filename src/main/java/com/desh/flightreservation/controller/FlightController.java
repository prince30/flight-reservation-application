package com.desh.flightreservation.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.desh.flightreservation.model.Flight;
import com.desh.flightreservation.service.FlightService;

@Controller
public class FlightController {
	@Autowired
	private FlightService flightService;

	@RequestMapping(value = "/findFlights", method = RequestMethod.GET)
	public String findFlights(@RequestParam("from") String from, @RequestParam("to") String to,
			@RequestParam("departureDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date departureDate,
			ModelMap modelMap) {
		List<Flight> flights = flightService.findFlights(from, to, departureDate);
		modelMap.addAttribute("flights", flights);
		return "flights/displayFlights";
	}
	
	@RequestMapping(value = "/admin/showAddFlight", method = RequestMethod.GET)
	public String showAddFlight() {
		return "flights/addFlight";
	}
}
