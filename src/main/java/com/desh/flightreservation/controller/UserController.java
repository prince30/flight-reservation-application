package com.desh.flightreservation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.desh.flightreservation.dto.UserRegistrationRequest;
import com.desh.flightreservation.service.SecurityService;
import com.desh.flightreservation.service.UserService;


@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private SecurityService securityService;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String create() {
		LOGGER.info("Inside showCreate()");
		return "users/registration";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String registration(@ModelAttribute("user") UserRegistrationRequest userRegistrationRequest) {
		LOGGER.info("Inside showLogin()" + userRegistrationRequest);
		userService.save(userRegistrationRequest);
		return "users/login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLogin() {
		LOGGER.info("Inside showLogin()");
		return "users/login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestParam("email") String email, @RequestParam("password") String password,
			ModelMap modelMap) {
		boolean loginResponse = securityService.login(email, password);
		LOGGER.info("Inside login() and the email is :" + email);
		if (loginResponse) {
			return "flights/findFlights";
		} else {
			modelMap.addAttribute("message", "Invalid username and password");
		}

		return "users/login";
	}
}
