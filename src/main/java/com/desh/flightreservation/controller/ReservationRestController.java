package com.desh.flightreservation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.desh.flightreservation.dto.ReservationUpdateRequest;
import com.desh.flightreservation.model.Reservation;
import com.desh.flightreservation.service.ReservationService;

@RestController
@CrossOrigin
public class ReservationRestController {

	@Autowired
	private ReservationService reservationService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReservationRestController.class);

	@RequestMapping(value = "/reservations/{id}", method = RequestMethod.GET)
	public Reservation findReservation(@PathVariable("id") Long id) {
		LOGGER.info("find reservation for Id: " + id);
		return reservationService.getById(id);
	}

	@RequestMapping(value = "/reservations", method = RequestMethod.POST)
	public Reservation updateReservation(@RequestBody ReservationUpdateRequest request) {
		LOGGER.info("inside update reservation for " + request);
		Reservation reservation = reservationService.getById(request.getId());
		reservation.setNumberOfBags(request.getNumberOfBags());
		reservation.setCheckedIn(request.getCheckedIn());
		LOGGER.info("saving reservation");
		return reservationService.save(reservation);
	}
}
