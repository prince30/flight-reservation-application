package com.desh.flightreservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desh.flightreservation.model.Passenger;


public interface PassengerRepository extends JpaRepository<Passenger, Long> {

}
