package com.desh.flightreservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.desh.flightreservation.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);
}
