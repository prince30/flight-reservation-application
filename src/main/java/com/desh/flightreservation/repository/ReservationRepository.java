package com.desh.flightreservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.desh.flightreservation.model.Reservation;


public interface ReservationRepository extends JpaRepository<Reservation, Long> {

}
