package com.desh.flightreservation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.desh.flightreservation.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
