package com.desh.flightreservation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.desh.flightreservation.dto.UserRegistrationRequest;
import com.desh.flightreservation.model.User;
import com.desh.flightreservation.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public User save(UserRegistrationRequest userRegistrationRequest) {

		User user = new User();
		
		user.setFirstName(userRegistrationRequest.getFirstName());
		user.setLastName(userRegistrationRequest.getLastName());
		user.setEmail(userRegistrationRequest.getEmail());
		user.setPassword(passwordEncoder.encode(userRegistrationRequest.getPassword()));

		return userRepository.save(user);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(username);

		if (user == null) {
			throw new UsernameNotFoundException("Invalid username and password");
		}

		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				user.getRoles());
	}
}
