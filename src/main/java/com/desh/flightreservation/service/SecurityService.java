package com.desh.flightreservation.service;

public interface SecurityService {
	boolean login(String username, String password);
}
