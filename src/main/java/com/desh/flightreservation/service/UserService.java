package com.desh.flightreservation.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import com.desh.flightreservation.dto.UserRegistrationRequest;
import com.desh.flightreservation.model.User;


public interface UserService extends UserDetailsService{

	User save(UserRegistrationRequest userRegistrationRequest);
}
