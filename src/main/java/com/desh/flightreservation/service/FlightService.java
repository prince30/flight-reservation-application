package com.desh.flightreservation.service;

import java.util.Date;
import java.util.List;
import com.desh.flightreservation.model.Flight;

public interface FlightService {

	List<Flight> findFlights(String from, String to, Date departureDate);
	
	Flight getById(Long Id);
}
