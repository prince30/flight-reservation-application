package com.desh.flightreservation.service;

import com.desh.flightreservation.dto.ReservationRequest;
import com.desh.flightreservation.model.Reservation;

public interface ReservationService {
	
	Reservation bookFlight(ReservationRequest request);
	
	Reservation getById(Long Id);
	
	Reservation save(Reservation reservation);
}
