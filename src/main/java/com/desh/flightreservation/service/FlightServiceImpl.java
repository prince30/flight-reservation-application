package com.desh.flightreservation.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desh.flightreservation.model.Flight;
import com.desh.flightreservation.repository.FlightRepository;

@Service
public class FlightServiceImpl implements FlightService {
	
	@Autowired
	private FlightRepository flightRepository;

	public List<Flight> findFlights(String from, String to, Date departureDate) {
		return flightRepository.findFlights(from, to, departureDate);
	}

	@Override
	public Flight getById(Long Id) {
		return flightRepository.findById(Id).get();
	}
}
