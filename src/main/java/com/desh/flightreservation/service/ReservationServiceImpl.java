package com.desh.flightreservation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.desh.flightreservation.dto.ReservationRequest;
import com.desh.flightreservation.model.Flight;
import com.desh.flightreservation.model.Passenger;
import com.desh.flightreservation.model.Reservation;
import com.desh.flightreservation.repository.FlightRepository;
import com.desh.flightreservation.repository.PassengerRepository;
import com.desh.flightreservation.repository.ReservationRepository;
import com.desh.flightreservation.util.EmailUtil;
import com.desh.flightreservation.util.PDFGenerator;

@Service
public class ReservationServiceImpl implements ReservationService {

	@Value("${com.desh.reservationflight.itinerary.dir.path}")
	private String ITINERARY_DIR_PATH;

	@Autowired
	FlightRepository flightRepository;

	@Autowired
	PassengerRepository passengerRepository;

	@Autowired
	ReservationRepository reservationRepository;

	@Autowired
	PDFGenerator pdfGenerator;

	@Autowired
	EmailUtil emailUtil;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReservationServiceImpl.class);

	@Override
	@Transactional
	public Reservation bookFlight(ReservationRequest request) {

		// Make payment
		// request.getCardNumber();

		Long flightId = request.getFlightId();
		LOGGER.info("Fetching flight for flight Id: " + flightId);
		Flight flight = flightRepository.findById(flightId).get();

		Passenger passenger = new Passenger();
		passenger.setFirstName(request.getPassengerFirstName());
		passenger.setLastName(request.getPassengerLastName());
		passenger.setEmail(request.getPassengerEmail());
		passenger.setPhone(request.getPassengerPhone());
		LOGGER.info("saving the passenger " + passenger);
		Passenger savedPassenger = passengerRepository.save(passenger);

		Reservation reservation = new Reservation();
		reservation.setFlight(flight);
		reservation.setPassenger(savedPassenger);
		reservation.setCheckedIn(false);
		LOGGER.info("saving the reservation " + reservation);
		Reservation savedReservation = reservationRepository.save(reservation);

		String filePath = ITINERARY_DIR_PATH + savedReservation.getId() + ".pdf";
		LOGGER.info("generating the itinerary.");
		pdfGenerator.generateItinerary(savedReservation, filePath);
		LOGGER.info("Emailing the itinerary.");
		emailUtil.sendItinerary(passenger.getEmail(), filePath);

		return savedReservation;
	}

	@Override
	public Reservation getById(Long Id) {
		return reservationRepository.findById(Id).get();
	}

	@Override
	public Reservation save(Reservation reservation) {
		return reservationRepository.save(reservation);
	}
}
